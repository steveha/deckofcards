package edu.ntnu.idatt2003;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Card game with a deck of cards.

 */
public class DeckOfCards {
    private final List<PlayingCard> cards = new ArrayList<>();
    private final Random random = new Random();

    public DeckOfCards() {
        char[] suits = { 'S', 'H', 'D', 'C' };
        for (char suit : suits) {
            for (int face = 1; face <= 13; face++) {
                cards.add(new PlayingCard(suit, face));
            }
        }
    }
    /**
     * Shuffles the deck of cards.
     */

    public void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     * Gives a hand of n cards.
     * If n is less than 1 or greater than the number of cards in the deck,
     * an IllegalArgumentException is thrown.
     * Picks n cards from the deck at random and returns them in a list.
     * @throws IllegalArgumentException if n is less than 1 or greater than the number of cards in the deck.
     * @param n number of cards to deal.
     * @return a hand of n cards.
     */
    public List<PlayingCard> dealHand(int n) {
        if (n < 1 || n > cards.size()) {
            throw new IllegalArgumentException("Invalid number of cards to deal.");
        }

        List<PlayingCard> hand = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int index = random.nextInt(cards.size());
            hand.add(cards.remove(index));
        }
        return hand;
    }

    /**
     * Returns the number of cards left in the deck.
     * @return the number of cards left in the deck.
     */

    public int cardsLeft() {
        return cards.size();
    }
}

