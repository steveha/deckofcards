package edu.ntnu.idatt2003;

import static javafx.application.Application.launch;

public class Main {
    public static void main(String[] args) {
        launch(CardGameGUI.class);
    }
}