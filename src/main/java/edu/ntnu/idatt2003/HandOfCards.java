package edu.ntnu.idatt2003;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A class representing a hand of cards.

 */
public class HandOfCards {
    private List<PlayingCard> hand;

    /**
     * Constructor for HandOfCards
     * @param hand A list of PlayingCard objects
     */

    public HandOfCards(List<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Method for calculating the sum of the face values of the cards in the hand
     * @return The sum of the face values
     */

    public int sumOfCardValues() {
        return hand.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    /**
     * Method for finding all the hearts in the hand
     * @return A string containing the hearts in the hand
     */
    public String heartsOnHand() {
        String hearts = hand.stream()
                .filter(card -> card.getSuit() == 'H')
                .map(PlayingCard::getAsString)
                .collect(Collectors.joining(" "));
        return hearts.isEmpty() ? "No Hearts" : hearts;
    }

    /**
     * Method for checking if the hand contains the queen of spades
     * @return True if the hand contains the queen of spades, false otherwise
     */
    public boolean hasQueenOfSpades() {
        return hand.stream()
                .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    /**
     * Method for checking if the hand contains a flush
     * @return True if the hand contains a flush, false otherwise
     */
    public boolean isFiveFlush() {
        return hand.stream()
                .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
                .values()
                .stream()
                .anyMatch(count -> count >= 5);
    }
    /**
     * Method for returning the hand as a string
     * @return A string representation of the hand
     */
    @Override
    public String toString() {
        return hand.stream()
                .map(PlayingCard::getAsString)
                .collect(Collectors.joining(" "));
    }
}

