package edu.ntnu.idatt2003;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CardGameGUI extends Application {

    private DeckOfCards deck;
    private HandOfCards hand;
    private TextArea handDisplay;
    private Label sumLabel;
    private Label heartsLabel;
    private Label flushLabel;
    private Label queenSpadesLabel;

    /**
     * Method for starting the application
     * Creates the GUI components
     * Creates the deck of cards
     * Sets the primary stage
     * @param primaryStage The primary stage
     */
    @Override
    public void start(Stage primaryStage) {

        deck = new DeckOfCards();
        deck.shuffle();

        handDisplay = new TextArea();
        Button dealHandButton = new Button("Deal hand");
        Button checkHandButton = new Button("Check hand");
        sumLabel = new Label("Sum of the faces: ");
        heartsLabel = new Label("Cards of hearts: ");
        flushLabel = new Label("Flush: No");
        queenSpadesLabel = new Label("Queen of spades: No");

        dealHandButton.setOnAction(event -> dealHand());

        checkHandButton.setOnAction(event -> checkHand());


        VBox root = new VBox(10, handDisplay, dealHandButton, checkHandButton, sumLabel, heartsLabel, flushLabel, queenSpadesLabel);
        Scene scene = new Scene(root, 300, 250);


        primaryStage.setTitle("Card Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method for dealing a hand of cards
     */
    private void dealHand() {

        hand = new HandOfCards(deck.dealHand(5));
        handDisplay.setText(hand.toString());
    }

    /**
     * Method for checking the hand of cards
     */

    private void checkHand() {

        sumLabel.setText("Sum of the faces: " + hand.sumOfCardValues());

        String hearts = hand.heartsOnHand();
        heartsLabel.setText("Cards of hearts: " + (hearts.isEmpty() ? "No Hearts" : hearts));

        flushLabel.setText("Flush: " + (hand.isFiveFlush() ? "Yes" : "No"));

        queenSpadesLabel.setText("Queen of spades: " + (hand.hasQueenOfSpades() ? "Yes" : "No"));
    }
}
