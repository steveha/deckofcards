import edu.ntnu.idatt2003.PlayingCard;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class PlayingCardTest {

    @Test
    public void testPlayingCardCreation() {
        PlayingCard card = new PlayingCard('H', 10);
        assertNotNull(card);
        assertEquals('H', card.getSuit());
        assertEquals(10, card.getFace());
    }

    @Test
    public void testGetAsString() {
        PlayingCard card = new PlayingCard('D', 12);
        assertEquals("D12", card.getAsString());
    }


}

