import static org.junit.Assert.*;

import edu.ntnu.idatt2003.DeckOfCards;
import edu.ntnu.idatt2003.PlayingCard;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DeckOfCardsTest {

    private DeckOfCards deck;

    @Before
    public void setUp() {
        deck = new DeckOfCards();
    }

    @Test
    public void testDeckSizeAfterCreation() {
        assertEquals(52, deck.cardsLeft());
    }

    @Test
    public void testShuffle() {
        deck.shuffle();
        List<PlayingCard> beforeShuffle = new ArrayList<>(deck.dealHand(52));

        deck = new DeckOfCards();
        deck.shuffle();

        List<PlayingCard> afterShuffle = new ArrayList<>(deck.dealHand(52));
        assertNotEquals(beforeShuffle.toString(), afterShuffle.toString());
    }

    @Test
    public void testDealHand() {
        deck.shuffle();
        List<PlayingCard> hand = deck.dealHand(5);
        assertEquals(5, hand.size());
        assertEquals(47, deck.cardsLeft());
    }

}
