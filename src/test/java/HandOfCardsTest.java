import static org.junit.Assert.*;

import edu.ntnu.idatt2003.HandOfCards;
import edu.ntnu.idatt2003.PlayingCard;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;


public class HandOfCardsTest {

    private HandOfCards hand;

    @Before
    public void setUp() {
        hand = new HandOfCards(Arrays.asList(
                new PlayingCard('H', 10),
                new PlayingCard('H', 11),
                new PlayingCard('H', 1),
                new PlayingCard('S', 12),
                new PlayingCard('D', 5)
        ));
    }

    @Test
    public void testSumOfCardValues() {
        assertEquals(39, hand.sumOfCardValues());
    }

    @Test
    public void testHeartsOnHand() {
        String expected = "H10 H11 H1";
        assertEquals(expected, hand.heartsOnHand());
    }

    @Test
    public void testHasQueenOfSpades() {
        assertTrue(hand.hasQueenOfSpades());
    }

    @Test
    public void testIsFiveFlush() {
        HandOfCards flushHand = new HandOfCards(Arrays.asList(
                new PlayingCard('D', 2),
                new PlayingCard('D', 3),
                new PlayingCard('D', 4),
                new PlayingCard('D', 5),
                new PlayingCard('D', 6)
        ));
        assertTrue(flushHand.isFiveFlush());
    }
}

